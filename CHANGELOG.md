# Change Log

## 0.5.0 - (unreleased)

### Added

- Added `line_type` parser.

### Changed

- Support expressions literals in amount position using curly braces, e.g.
  {1000 10 +} EUR. The format of the expression language is not specified.

- Allow both unit price and total price in closing position to be
  specified, e.g. "0.5 BTC @ 7000 EUR = 3500 EUR". This allows it to
  both track the unit price as well as the value used for balancing.

## 0.4.0 - 2019-09-01

### Added

- Added ClosingPositionType enum.

- Added `PostingType` enum.

### Changed

- `ClosingPosition` is now a struct instead of an enum.

- `DateTime` and `Timezone` implement `Copy` and `Clone`.

- Changed `Posting` struct. Added `posting_type` field of type
  `PostingTyp` and removed `AccountName::{Regular, Virtual}`. The
  posting type should not be part of the account name. This simplifies
  some code in `hauptbuch-core`.

### Removed

- `account_name::AccountName`.

## 0.3.0 - 2019-08-31

### Added

- Parses now file comments. Any line that starts with ";" is now
  considered a comment and is ignored.

### Changed

- `Position#amount` is now a `&str` instead of a
  `rust_decimal::Decimal`. Callers are now responsible for converting
  into whatever representation they want.

- Module `models` is no longer. The corresponding model has moved
  into the same submodule as the parsing function. For example, the
  `Line` model is in `hauptbuch-parser::line::Line` and the line parser
  is `hauptbuch-parser::line::line`.

- The `parsers` subdirectory is gone.

### Removed

- Dropped dependency on `rust_decimal`.

- Dropped support for [combine][combine-parser] parser. Maintaining two
  parsers was a lot of duplicate work. [Nom][nom-parser] is nearly twice
  as fast.

- `LineProcessor` and `LineVisitor` were moved to crate
  `hauptbuch-core`.

## 0.2.0 - 2019-08-31

This is the last release that supported two parsers, [nom][nom-parser]
and [combine][combine-parser].

[combine-parser]: https://github.com/Marwes/combine
[nom-parser]: https://github.com/Geal/nom
