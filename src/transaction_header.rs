use crate::{
    code::code,
    datetime::{datetime, DateTime},
    description::description,
    status_mark::{status_mark, StatusMark},
};
use nom::{
    character::complete::space1,
    combinator::{map, opt},
    sequence::{preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub struct TransactionHeader<'a> {
    pub datetime: DateTime,
    pub status_mark: StatusMark,
    pub code: Option<&'a str>,
    pub description: &'a str,
}

pub(crate) fn transaction_header(i: &str) -> IResult<&str, TransactionHeader> {
    map(
        tuple((
            datetime,
            map(opt(preceded(space1, status_mark)), |r| {
                r.unwrap_or(StatusMark::Unmarked)
            }),
            opt(preceded(space1, code)),
            preceded(space1, description),
        )),
        |(datetime, status_mark, code, description)| TransactionHeader {
            datetime,
            status_mark,
            code,
            description,
        },
    )(i)
}

#[test]
fn it_should_parse_transaction_header_with_code() {
    use chrono::NaiveDate;
    assert_eq!(
        transaction_header("2018-05-06 (code)  'The description "),
        Ok((
            "",
            TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2018, 5, 6)),
                status_mark: StatusMark::Unmarked,
                code: Some("code"),
                description: "The description",
            }
        ))
    );
}

#[test]
fn it_should_parse_transaction_header_without_code() {
    use chrono::NaiveDate;
    assert_eq!(
        transaction_header("2018-05-12 'Description"),
        Ok((
            "",
            TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2018, 5, 12)),
                status_mark: StatusMark::Unmarked,
                code: None,
                description: "Description"
            }
        ))
    );
}

#[test]
fn it_should_parse_transaction_header_with_status_mark() {
    use chrono::NaiveDate;
    assert_eq!(
        transaction_header("2018-05-06 !   'The description "),
        Ok((
            "",
            TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2018, 5, 6)),
                status_mark: StatusMark::Pending,
                code: None,
                description: "The description",
            }
        ))
    );
}

#[test]
fn it_should_parse_transaction_header_with_status_mark_and_code() {
    use chrono::NaiveDate;
    assert_eq!(
        transaction_header("2018-05-06 ! (#abc)  'The description "),
        Ok((
            "",
            TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2018, 5, 6)),
                status_mark: StatusMark::Pending,
                code: Some("#abc"),
                description: "The description",
            }
        ))
    );
    assert_eq!(
        transaction_header("2018-05-06 * (#abc)  'The description "),
        Ok((
            "",
            TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2018, 5, 6)),
                status_mark: StatusMark::Cleared,
                code: Some("#abc"),
                description: "The description",
            }
        ))
    );
}

#[test]
fn it_should_fail_given_invalid_spacing() {
    assert!(transaction_header("2018-05-06!(#abc)  'The description ").is_err());

    assert!(transaction_header("2018-05-06 !(#abc)  'The description ").is_err());

    assert!(transaction_header("2018-05-06 ! (#abc)'The description ").is_err());

    assert!(transaction_header("2018-05-06(#abc)  'The description ").is_err());

    assert!(transaction_header("2018-05-06 (#abc)'The description ").is_err());

    assert!(transaction_header("2018-05-06'The description ").is_err());
}

#[test]
fn it_should_fail_given_transaction_header_with_invalid_code_missing_whitespace() {
    assert!(transaction_header("2018-05-12 (code)'Description").is_err());
}

#[test]
fn it_should_fail_given_transaction_header_with_invalid_description() {
    assert!(transaction_header("2018-05-12 (code) Description").is_err());
}
