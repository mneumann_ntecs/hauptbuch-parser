use nom::{branch::alt, character::complete::char, combinator::map, IResult};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum StatusMark {
    /// No mark
    Unmarked,
    /// `!`
    Pending,
    /// `*`
    Cleared,
}

pub(crate) fn status_mark(i: &str) -> IResult<&str, StatusMark> {
    alt((
        map(char('!'), |_| StatusMark::Pending),
        map(char('*'), |_| StatusMark::Cleared),
    ))(i)
}

#[test]
fn it_should_parse_valid_status_marks() {
    assert_eq!(status_mark("!"), Ok(("", StatusMark::Pending)));
    assert_eq!(status_mark("*"), Ok(("", StatusMark::Cleared)));
}

#[test]
fn it_should_fail_otherwise() {
    assert!(status_mark("").is_err());
    assert!(status_mark("(").is_err());
}
