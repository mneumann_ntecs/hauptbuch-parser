use nom::{
    character::complete::{char, none_of},
    combinator::{map, recognize},
    multi::many1,
    sequence::delimited,
    IResult,
};

pub(crate) fn code(i: &str) -> IResult<&str, &str> {
    map(
        delimited(char('('), recognize(many1(none_of("'()[]{}<>"))), char(')')),
        str::trim,
    )(i)
}

#[test]
fn it_should_parse_valid_codes() {
    assert_eq!(code("(code)"), Ok(("", "code")));
    assert_eq!(code("(code) "), Ok((" ", "code")));
    assert_eq!(code("(a b c) "), Ok((" ", "a b c")));
}

#[test]
fn it_should_parse_valid_utf8_code() {
    assert_eq!(code("(jäätelö)"), Ok(("", "jäätelö")));
    assert_eq!(code("(アイスクリーム)"), Ok(("", "アイスクリーム")));
    assert_eq!(code("(風:空)"), Ok(("", "風:空")));
}

#[test]
fn it_should_parse_valid_code_and_remove_any_trailing_spaces() {
    assert_eq!(code("(code a  ) "), Ok((" ", "code a")));
}

#[test]
fn it_should_parse_valid_code_and_remove_any_leading_spaces() {
    assert_eq!(code("(   code a) "), Ok((" ", "code a")));
}

#[test]
fn it_should_parse_valid_code_and_remove_any_leading_and_trailing_spaces() {
    assert_eq!(code("(   code a   ) "), Ok((" ", "code a")));
}

#[test]
fn it_should_fail_on_invalid_codes_starting_with_space() {
    assert!(code(" (code)").is_err());
}

#[test]
fn it_should_fail_on_invalid_codes() {
    assert!(code("(a b c (d e) ").is_err());
    assert!(code("(abc () ").is_err());
    assert!(code("(abc ') ").is_err());
    assert!(code("(abc <) ").is_err());
    assert!(code("(abc >) ").is_err());
    assert!(code("(abc {) ").is_err());
    assert!(code("(abc }) ").is_err());
    assert!(code("(abc [) ").is_err());
    assert!(code("(abc ]) ").is_err());
}
