use crate::helpers::digits::{dec_digits_2, dec_digits_4};
use chrono::NaiveDate;
use nom::{
    character::complete::char,
    combinator::map_opt,
    sequence::{preceded, tuple},
    IResult,
};

pub(crate) fn date(i: &str) -> IResult<&str, NaiveDate> {
    map_opt(
        tuple((
            dec_digits_4,
            preceded(char('-'), dec_digits_2),
            preceded(char('-'), dec_digits_2),
        )),
        |(year, month, day): (u16, u8, u8)| {
            NaiveDate::from_ymd_opt(year.into(), month.into(), day.into())
        },
    )(i)
}

#[test]
fn it_should_parse_valid_dates() {
    assert_eq!(
        date("2018-08-15"),
        Ok(("", NaiveDate::from_ymd(2018, 8, 15)))
    );
    assert_eq!(
        date("2018-08-15 "),
        Ok((" ", NaiveDate::from_ymd(2018, 8, 15)))
    );
    assert_eq!(
        date("2018-08-15  (code)"),
        Ok(("  (code)", NaiveDate::from_ymd(2018, 8, 15))),
    );
}

#[test]
fn it_should_fail_on_invalid_date_syntax() {
    let invalid = [" 2018-08-15 ", "2018-0-1", "2···-··-··"];
    for &v in &invalid {
        assert!(date(v).is_err());
    }
}

#[test]
fn it_should_fail_on_dates_out_of_range() {
    let invalid = [
        "2018-00-01",
        "2018-13-01",
        "2018-01-32",
        "2018-02-29",
        "2018-04-31",
        "2018-01-00",
    ];
    for &v in &invalid {
        assert!(date(v).is_err());
    }
}
