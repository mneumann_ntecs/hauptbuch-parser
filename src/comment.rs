use nom::{
    branch::alt,
    character::complete::{char, space1},
    combinator::{map, rest},
    sequence::{preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub enum Comment<'a> {
    UserComment(&'a str),
    Attribute(&'a str),
}

pub fn comment(i: &str) -> IResult<&str, Comment> {
    alt((
        parse_comment(';', Comment::UserComment),
        parse_comment('#', Comment::Attribute),
    ))(i)
}

fn parse_comment<'a>(
    prefix: char,
    constructor: impl Fn(&'a str) -> Comment<'a>,
) -> impl Fn(&'a str) -> IResult<&'a str, Comment> {
    move |i: &str| {
        map(preceded(tuple((char(prefix), space1)), rest), |r: &str| {
            constructor(r.trim_end())
        })(i)
    }
}

#[test]
fn it_should_parse_user_comments() {
    assert_eq!(
        comment("; This is a user comment"),
        Ok(("", Comment::UserComment("This is a user comment")))
    );
}

#[test]
fn it_should_parse_attributes() {
    assert_eq!(
        comment("# uuid: 83976d4b-8ea8-4cec-804f-931e4f171c3b"),
        Ok((
            "",
            Comment::Attribute("uuid: 83976d4b-8ea8-4cec-804f-931e4f171c3b")
        ))
    );
}

#[test]
fn it_should_fail_given_no_leading_space() {
    assert!(comment(";This is no user comment").is_err());
    assert!(comment("#This is no user comment").is_err());
}

#[test]
fn it_should_drop_trailing_spaces() {
    assert_eq!(
        comment("; This is a user comment      "),
        Ok(("", Comment::UserComment("This is a user comment")))
    );
    assert_eq!(
        comment("# This is a attribute      "),
        Ok(("", Comment::Attribute("This is a attribute")))
    );
}

#[test]
fn it_should_drop_leading_spaces() {
    assert_eq!(
        comment(";   This is a user comment"),
        Ok(("", Comment::UserComment("This is a user comment")))
    );
    assert_eq!(
        comment("#   This is a attribute"),
        Ok(("", Comment::Attribute("This is a attribute")))
    );
}
