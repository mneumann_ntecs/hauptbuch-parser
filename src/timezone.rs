use crate::helpers::digits::dec_digits_2;
use nom::{
    branch::alt,
    character::complete::{char, one_of},
    combinator::{map, map_res},
    sequence::{preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Timezone {
    Utc,
    FixedOffset(i16),
}

pub(crate) fn timezone(i: &str) -> IResult<&str, Timezone> {
    alt((
        map(char('Z'), |_| Timezone::Utc),
        map_res(
            tuple((
                map(one_of("+-"), |c: char| if c == '+' { 1i16 } else { -1i16 }),
                dec_digits_2,
                preceded(char(':'), dec_digits_2),
            )),
            |(sign, hour, min)| {
                let offset_in_minutes = (hour as i16 * 60) + (min as i16);
                if hour > 23 || min > 59 || offset_in_minutes > (23 * 60) + 59 {
                    Err(())
                } else {
                    Ok(Timezone::FixedOffset(sign * offset_in_minutes))
                }
            },
        ),
    ))(i)
}

#[test]
fn it_should_parse_valid_timezones() {
    assert_eq!(timezone("Z"), Ok(("", Timezone::Utc)));
    assert_eq!(timezone("+12:00"), Ok(("", Timezone::FixedOffset(720))));
    assert_eq!(timezone("-01:00"), Ok(("", Timezone::FixedOffset(-60))));
}

#[test]
fn it_should_fail_on_invalid_timezones() {
    assert!(timezone("+23:60").is_err());
    assert!(timezone("+22:60").is_err());
    assert!(timezone("+24:00").is_err());
    assert!(timezone("-24:00").is_err());
}
