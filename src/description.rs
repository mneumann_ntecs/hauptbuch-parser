use nom::{
    character::complete::char,
    combinator::{map, rest},
    sequence::preceded,
    IResult,
};

pub(crate) fn description(i: &str) -> IResult<&str, &str> {
    map(preceded(char('\''), rest), str::trim_end)(i)
}

#[test]
fn it_should_parse_descriptions() {
    assert_eq!(description("'Description"), Ok(("", "Description")));
}

#[test]
fn it_should_drop_trailing_spaces() {
    assert_eq!(description("'Description   "), Ok(("", "Description")));
}

#[test]
fn it_should_fail_to_parse_invalid_descriptions() {
    assert!(description(" 'Description with leading space should fail").is_err());

    assert!(description("Description without leading apostroph should fail").is_err());
}
