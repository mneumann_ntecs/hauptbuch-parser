use nom::{
    branch::alt,
    character::complete::{char, digit1, none_of},
    combinator::{map, opt, recognize},
    multi::many0_count,
    sequence::{delimited, pair, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub enum Amount<'a> {
    /// Any number, e.g. "123" or "1.333"
    Numeric(&'a str),

    /// The inner part of "{123 / 33}", i.e. "123 / 33".
    Expression(&'a str),
}

pub(crate) fn amount(i: &str) -> IResult<&str, Amount> {
    alt((
        map(numeric_amount, |amt| Amount::Numeric(amt)),
        map(expression_amount, |expr| Amount::Expression(expr)),
    ))(i)
}

fn numeric_amount(s: &str) -> IResult<&str, &str> {
    recognize(tuple((
        opt(char('-')),
        digit1,
        opt(pair(char('.'), digit1)),
    )))(s)
}

fn expression_amount(s: &str) -> IResult<&str, &str> {
    // delimited(char('{'), take_until(char('}')), char('}'))(s)
    delimited(char('{'), recognize(many0_count(none_of("}"))), char('}'))(s)
}

#[test]
fn it_should_parse_valid_numeric_amounts() {
    let valid = ["1", "-123", "-123.234"];

    for &v in &valid {
        assert_eq!(amount(v), Ok(("", Amount::Numeric(v))));
    }
}

#[test]
fn it_should_parse_expression_amounts() {
    assert_eq!(amount("{1 + 2}"), Ok(("", Amount::Expression("1 + 2"))));
}
