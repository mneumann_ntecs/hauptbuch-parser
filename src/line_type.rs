use crate::helpers::character_complete_ext::{alpha_char, eos, single_space};
use nom::{
    branch::alt,
    character::complete::{char, line_ending, one_of, space1},
    combinator::{map, opt, peek},
    sequence::{pair, preceded},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub enum LineType {
    /// An empty line
    EmptyLine,

    /// Start of a new transaction
    TransactionHeader,

    /// A posting within a transaction
    Posting,

    /// A comment occuring within the boundary of a transaction
    TransactionComment,

    /// An attribute occuring within the boundary of a transaction
    TransactionAttribute,

    /// A comment outside the boundary of a transaction
    FileComment,
}

pub fn line_type(i: &str) -> IResult<&str, LineType> {
    alt((
        map(pair(char(';'), single_space), |_| LineType::FileComment),
        map(peek(one_of("0123456789")), |_| LineType::TransactionHeader),
        map(pair(opt(line_ending), eos), |_| LineType::EmptyLine),
        preceded(
            space1,
            alt((
                map(pair(char(';'), single_space), |_| {
                    LineType::TransactionComment
                }),
                map(pair(char('#'), single_space), |_| {
                    LineType::TransactionAttribute
                }),
                map(peek(alpha_char), |_| LineType::Posting),
                map(pair(opt(line_ending), eos), |_| LineType::EmptyLine),
            )),
        ),
    ))(i)
}

#[test]
fn it_should_parse_empty_lines() {
    assert_eq!(line_type(""), Ok(("", LineType::EmptyLine)));
    assert_eq!(line_type("    "), Ok(("", LineType::EmptyLine)));
}

#[test]
fn it_should_ignore_line_ending() {
    assert_eq!(line_type("\n"), Ok(("", LineType::EmptyLine)));
    assert_eq!(line_type("   \n"), Ok(("", LineType::EmptyLine)));
    assert_eq!(line_type("   \r\n"), Ok(("", LineType::EmptyLine)));
}

#[test]
fn it_should_parse_file_comments() {
    assert_eq!(
        line_type("; 2017-07-07 (ok) 'From a to b   "),
        Ok(("2017-07-07 (ok) 'From a to b   ", LineType::FileComment))
    );
}

#[test]
fn it_should_parse_transaction_header() {
    assert_eq!(
        line_type("2017-07-07 (ok) 'From a to b   "),
        Ok((
            "2017-07-07 (ok) 'From a to b   ",
            LineType::TransactionHeader
        ))
    );
}

#[test]
fn it_should_parse_transaction_comment() {
    assert_eq!(
        line_type(" ; This is a transaction comment  "),
        Ok((
            "This is a transaction comment  ",
            LineType::TransactionComment
        ))
    );
}

#[test]
fn it_should_parse_transaction_attribute() {
    assert_eq!(
        line_type("    # uuid: 83976d4b-8ea8-4cec-804f-931e4f171c3b"),
        Ok((
            "uuid: 83977d4b-8ea8-4cec-804f-931e4f171c3b",
            LineType::TransactionAttribute,
        ))
    );
}

#[test]
fn it_should_parse_posting() {
    assert_eq!(
        line_type(" Expenses:Ice·Cream  200 SEK @ 0.1039 EUR"),
        Ok((
            "Expenses:Ice·Cream  200 SEK @ 0.1039 EUR",
            LineType::Posting
        ))
    );
}

#[test]
fn it_should_fail_to_parse_posting_if_not_indented() {
    assert!(line_type("Expenses:Ice·Cream  200 SEK @ 0.1039 EUR").is_err());
}

#[test]
fn it_should_fail_to_parse_transaction_header_if_indented() {
    assert!(line_type(" 2017-07-07 (ok) 'From a to b   ").is_err());
}

#[test]
fn it_should_fail_to_parse_comments_if_no_trailing_space() {
    assert!(line_type(";test").is_err());
    assert!(line_type(" ;test").is_err());
    assert!(line_type(" #test").is_err());
}
