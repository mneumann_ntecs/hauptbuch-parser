use crate::{
    amount::{amount, Amount},
    commodity::commodity,
};
use nom::{
    character::complete::space1,
    combinator::{map, opt},
    sequence::{pair, preceded},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub struct Position<'a> {
    pub amount: Amount<'a>,
    pub commodity: Option<&'a str>,
}

pub(crate) fn position(i: &str) -> IResult<&str, Position> {
    map(
        pair(amount, opt(preceded(space1, commodity))),
        |(amount, commodity)| Position { amount, commodity },
    )(i)
}

#[test]
fn it_should_parse_positions() {
    assert_eq!(
        position("123 EUR"),
        Ok((
            "",
            Position {
                amount: Amount::Numeric("123"),
                commodity: Some("EUR")
            }
        ))
    );

    assert_eq!(
        position("-1.123 ACME·INC"),
        Ok((
            "",
            Position {
                amount: Amount::Numeric("-1.123"),
                commodity: Some("ACME·INC")
            }
        ))
    );

    assert_eq!(
        position("-1.123"),
        Ok((
            "",
            Position {
                amount: Amount::Numeric("-1.123"),
                commodity: None
            }
        ))
    );
}
