use crate::position::{position, Position};
use nom::{
    character::complete::{char, space1},
    combinator::{map, opt},
    sequence::{preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub struct PostingValue<'a> {
    pub value_position: Position<'a>,
    pub closing_unit_price: Option<Position<'a>>,
    pub closing_total_price: Option<Position<'a>>,
}

pub(crate) fn posting_value(i: &str) -> IResult<&str, PostingValue> {
    map(
        tuple((
            position,
            opt(preceded(tuple((space1, char('@'), space1)), position)),
            opt(preceded(tuple((space1, char('='), space1)), position)),
        )),
        |(value_position, closing_unit_price, closing_total_price)| PostingValue {
            value_position,
            closing_unit_price,
            closing_total_price,
        },
    )(i)
}

#[test]
fn it_should_parse_posting_values_without_closing_position() {
    use crate::amount::Amount;
    assert_eq!(
        posting_value("2.12"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("2.12"),
                    commodity: None
                },
                closing_unit_price: None,
                closing_total_price: None,
            }
        ))
    );

    assert_eq!(
        posting_value("123 EUR"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: None,
                closing_total_price: None,
            }
        ))
    );
}

#[test]
fn it_should_parse_posting_values_with_closing_position_unit_price() {
    use crate::amount::Amount;
    assert_eq!(
        posting_value("123 EUR @ 1 SEK"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: Some(Position {
                    amount: Amount::Numeric("1"),
                    commodity: Some("SEK")
                }),
                closing_total_price: None,
            }
        ))
    );

    assert_eq!(
        posting_value("123 EUR @ 1"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: Some(Position {
                    amount: Amount::Numeric("1"),
                    commodity: None
                }),
                closing_total_price: None,
            }
        ))
    );
}

#[test]
fn it_should_parse_posting_values_with_closing_position_total_price() {
    use crate::amount::Amount;
    assert_eq!(
        posting_value("123 EUR = 1 SEK"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: None,
                closing_total_price: Some(Position {
                    amount: Amount::Numeric("1"),
                    commodity: Some("SEK")
                }),
            }
        ))
    );

    assert_eq!(
        posting_value("123 EUR = 1"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: None,
                closing_total_price: Some(Position {
                    amount: Amount::Numeric("1"),
                    commodity: None
                }),
            }
        ))
    );
}

#[test]
fn it_should_parse_posting_values_with_closing_unit_price_and_total() {
    use crate::amount::Amount;
    assert_eq!(
        posting_value("123 EUR @ 1 SEK = 123 SEK"),
        Ok((
            "",
            PostingValue {
                value_position: Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("EUR")
                },
                closing_unit_price: Some(Position {
                    amount: Amount::Numeric("1"),
                    commodity: Some("SEK")
                }),
                closing_total_price: Some(Position {
                    amount: Amount::Numeric("123"),
                    commodity: Some("SEK")
                }),
            }
        ))
    );
}
