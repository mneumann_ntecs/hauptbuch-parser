fn is_middledot(c: char) -> bool {
    c == '\u{00b7}' // '·'
}

pub(crate) fn is_interpunct(c: char) -> bool {
    c == '-' || c == '_' || is_middledot(c)
}

pub(crate) fn is_currency_symbol(c: char) -> bool {
    /* Dollar $ */
    c == '\u{0024}' ||
    /* Euro */     c == '\u{0080}' ||
    /* Pound */    c == '\u{00A3}' ||
    /* Yen */      c == '\u{00A5}' ||
    /* UTF8 */    (c >= '\u{20A0}' && c <= '\u{20CF}')
}
