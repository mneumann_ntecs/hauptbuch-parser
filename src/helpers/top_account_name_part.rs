use crate::helpers::{
    character_complete_ext::{alpha_char, take_while_cond},
    symbols::is_interpunct,
};
use nom::{combinator::recognize, sequence::pair, IResult};

// Top level account names begin with an UTF-8 letter followed by letters, digits, middle dot,
// hyphen and underscore. No spaces are allowed as part of an account name.

pub(crate) fn top_account_name_part(i: &str) -> IResult<&str, &str> {
    recognize(pair(
        alpha_char,
        take_while_cond(|c: char| c.is_alphanumeric() || is_interpunct(c)),
    ))(i)
}

#[test]
fn it_should_parse_valid_top_account_name_parts() {
    let valid = [
        "Expenses",
        "ice_cream",
        "jäätelö",
        "mansikka-vadelma",
        "crème·glacée",
        "мороженое",
        "アイスクリーム",
        "風",
        "空",
        "assets",
        "cash",
        "Income2018",
    ];

    for &v in &valid {
        assert_eq!(top_account_name_part(v), Ok(("", v)));
    }
}

#[test]
fn it_should_fail_given_invalid_top_account_name_part() {
    let invalid = ["2018", "2018Income", "1cash", "-cash"];
    for &v in &invalid {
        assert!(top_account_name_part(v).is_err());
    }
}
