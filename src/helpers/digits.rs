use crate::helpers::character_complete_ext::take_n_cond;
use nom::{combinator::map_opt, error::ErrorKind, IResult};
use std::str::FromStr;

pub(crate) fn dec_digits_2(i: &str) -> IResult<&str, u8> {
    dec_digits_n::<u8>(i, 2)
}

pub(crate) fn dec_digits_4(i: &str) -> IResult<&str, u16> {
    dec_digits_n::<u16>(i, 4)
}

pub(crate) fn dec_digits_9(i: &str) -> IResult<&str, u32> {
    dec_digits_n::<u32>(i, 9)
}
fn is_dec_digit(c: char) -> bool {
    c.is_digit(10)
}

fn dec_digits_n<T: FromStr + Copy>(i: &str, n: usize) -> IResult<&str, T> {
    map_opt(take_n_cond(n, is_dec_digit, ErrorKind::Digit), |s: &str| {
        T::from_str(s).ok()
    })(i)
}
