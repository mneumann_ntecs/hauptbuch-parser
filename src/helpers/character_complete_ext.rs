use nom::{
    character::complete::{anychar, one_of},
    combinator::{recognize, verify},
    IResult,
    {error::ErrorKind, Err},
};

pub(crate) fn take_n_cond(
    count: usize,
    cond: impl Fn(char) -> bool,
    error_kind: ErrorKind,
) -> impl Fn(&str) -> IResult<&str, &str>
where
{
    move |i: &str| {
        let mut iter = i.chars();
        let mut cnt = count;
        let mut split_pos = 0;

        while cnt > 0 {
            if let Some(ch) = iter.next() {
                if cond(ch) {
                    // ok
                    cnt -= 1;
                    split_pos += ch.len_utf8();
                } else {
                    break;
                }
            } else {
                break;
            }
        }

        if cnt > 0 {
            Err(Err::Error((iter.as_str(), error_kind)))
        } else {
            Ok(swap(i.split_at(split_pos)))
        }
    }
}

pub(crate) fn take_while_cond(cond: impl Fn(char) -> bool) -> impl Fn(&str) -> IResult<&str, &str> {
    move |i: &str| {
        let mut iter = i.chars();
        let mut split_pos = 0;

        loop {
            if let Some(ch) = iter.next() {
                if cond(ch) {
                    split_pos += ch.len_utf8();
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        return Ok(swap(i.split_at(split_pos)));
    }
}

pub(crate) fn alpha_char(i: &str) -> IResult<&str, &str> {
    recognize(verify(anychar, |c: &char| c.is_alphabetic()))(i)
}

pub(crate) fn alphanumeric_char(i: &str) -> IResult<&str, &str> {
    recognize(verify(anychar, |c: &char| c.is_alphanumeric()))(i)
}

pub(crate) fn eos(i: &str) -> IResult<&str, &str> {
    if i.len() == 0 {
        Ok((i, i))
    } else {
        Err(Err::Error((i, ErrorKind::Eof)))
    }
}

pub(crate) fn single_space(i: &str) -> IResult<&str, char> {
    one_of(" \t")(i)
}

fn swap<'a, 'b>((a, b): (&'a str, &'b str)) -> (&'b str, &'a str) {
    (b, a)
}
