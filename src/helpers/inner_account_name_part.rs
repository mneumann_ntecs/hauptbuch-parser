use crate::helpers::{
    character_complete_ext::{alphanumeric_char, take_while_cond},
    symbols::is_interpunct,
};
use nom::{combinator::recognize, sequence::pair, IResult};

// Inner account names begin with an UTF-8 letter OR digits, followed by letters, digits, middle
// dot, hyphen and underscore. No spaces are allowed as part of an account name.

pub(crate) fn inner_account_name_part(i: &str) -> IResult<&str, &str> {
    recognize(pair(
        alphanumeric_char,
        take_while_cond(|c: char| c.is_alphanumeric() || is_interpunct(c)),
    ))(i)
}

#[test]
fn it_should_parse_valid_inner_account_name_parts() {
    let valid = [
        "Expenses",
        "ice_cream",
        "jäätelö",
        "mansikka-vadelma",
        "crème·glacée",
        "мороженое",
        "アイスクリーム",
        "風",
        "空",
        "assets",
        "cash",
        "Income2018",
        "2018",
        "2018-Income",
    ];

    for &v in &valid {
        assert_eq!(inner_account_name_part(v), Ok(("", v)));
    }
}

#[test]
fn it_should_fail_given_invalid_inner_account_name_part() {
    let invalid = ["-1cash"];
    for &v in &invalid {
        assert!(inner_account_name_part(v).is_err());
    }
}
