use crate::{
    date::date,
    time::time,
    timezone::{timezone, Timezone},
};
use chrono::{NaiveDate, NaiveDateTime};
use nom::{
    character::complete::char,
    combinator::{map, opt},
    sequence::{pair, preceded, tuple},
    IResult,
};

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum DateTime {
    Date(NaiveDate),
    DateTime(NaiveDateTime),
    DateTimeTz(NaiveDateTime, Timezone),
}

// ISO-8601 date/time with nanosecond resolution and timezone
pub(crate) fn datetime(i: &str) -> IResult<&str, DateTime> {
    map(
        tuple((date, opt(preceded(char('T'), pair(time, opt(timezone)))))),
        |(date, timetz)| match timetz {
            Some((time, Some(timezone))) => DateTime::DateTimeTz(date.and_time(time), timezone),
            Some((time, None)) => DateTime::DateTime(date.and_time(time)),
            None => DateTime::Date(date),
        },
    )(i)
}

#[test]
fn it_should_parse_valid_datetimes() {
    assert_eq!(
        datetime("2016-12-31"),
        Ok(("", DateTime::Date(NaiveDate::from_ymd(2016, 12, 31))))
    );
    assert_eq!(
        datetime("2016-12-31T13:01:01"),
        Ok((
            "",
            DateTime::DateTime(NaiveDate::from_ymd(2016, 12, 31).and_hms(13, 1, 1))
        ))
    );
    assert_eq!(
        datetime("2016-12-31T13:01:01.123456789"),
        Ok((
            "",
            DateTime::DateTime(NaiveDate::from_ymd(2016, 12, 31).and_hms_nano(13, 1, 1, 123456789))
        ))
    );
    assert_eq!(
        datetime("2016-12-31T13:01:01Z"),
        Ok((
            "",
            DateTime::DateTimeTz(
                NaiveDate::from_ymd(2016, 12, 31).and_hms(13, 1, 1),
                Timezone::Utc
            )
        ))
    );
    assert_eq!(
        datetime("2016-12-31T13:01:01+02:00"),
        Ok((
            "",
            DateTime::DateTimeTz(
                NaiveDate::from_ymd(2016, 12, 31).and_hms(13, 1, 1),
                Timezone::FixedOffset(120)
            )
        ))
    );

    assert_eq!(
        datetime("2016-12-31T13:01:01.123456789+02:00"),
        Ok((
            "",
            DateTime::DateTimeTz(
                NaiveDate::from_ymd(2016, 12, 31).and_hms_nano(13, 1, 1, 123456789),
                Timezone::FixedOffset(120)
            )
        ))
    );
}
