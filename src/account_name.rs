use crate::helpers::{
    inner_account_name_part::inner_account_name_part, top_account_name_part::top_account_name_part,
};
use nom::{
    character::complete::char,
    combinator::recognize,
    multi::many0_count,
    sequence::{pair, preceded},
    IResult,
};

// Account names are separated by ":"
pub(crate) fn account_name(i: &str) -> IResult<&str, &str> {
    recognize(pair(
        top_account_name_part,
        many0_count(preceded(char(':'), inner_account_name_part)),
    ))(i)
}

#[cfg(test)]
const VALID_ACCOUNT_NAMES: &[&str] = &[
    "Expenses:ice_cream",
    "Expenses:jäätelö",
    "Expenses:jäätelö:mansikka-vadelma",
    "Expenses:crème·glacée",
    "Expenses:мороженое",
    "Expenses:アイスクリーム",
    "Expenses:風:空",
    "assets:cash",
    "Income:2018",
];

#[test]
fn it_should_parse_valid_regular_account_names() {
    for &v in VALID_ACCOUNT_NAMES {
        assert_eq!(account_name(v), Ok(("", v)));
    }
}

#[test]
fn it_should_stop_parsing_account_name_upon_first_space() {
    assert_eq!(account_name("test 123"), Ok((" 123", "test")));
}
