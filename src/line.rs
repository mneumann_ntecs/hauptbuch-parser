use crate::{
    comment::{comment, Comment},
    posting::{posting, Posting},
    transaction_header::{transaction_header, TransactionHeader},
};
use nom::{
    branch::alt,
    character::complete::{char, line_ending, space0, space1},
    combinator::{map, opt, rest},
    exact,
    sequence::{preceded, terminated},
    IResult,
};

#[derive(Debug, PartialEq, Eq)]
pub enum Line<'a> {
    Empty,
    TransactionHeader(TransactionHeader<'a>),
    TransactionComment(Comment<'a>),
    FileComment(&'a str),
    Posting(Posting<'a>),
}

pub fn line(i: &str) -> IResult<&str, Line> {
    exact!(
        i,
        terminated(
            alt((
                map(preceded(space1, posting), Line::Posting),
                map(transaction_header, Line::TransactionHeader),
                map(preceded(space1, comment), Line::TransactionComment),
                map(preceded(char(';'), rest), |r: &str| Line::FileComment(
                    r.trim()
                )),
                map(space0, |_| Line::Empty),
            )),
            opt(line_ending),
        )
    )
}

#[test]
fn it_should_parse_empty_lines() {
    assert_eq!(line(""), Ok(("", Line::Empty)));
    assert_eq!(line("    "), Ok(("", Line::Empty)));
}

#[test]
fn it_should_ignore_line_ending() {
    assert_eq!(line("\n"), Ok(("", Line::Empty)));
    assert_eq!(line("   \n"), Ok(("", Line::Empty)));
    assert_eq!(line("   \r\n"), Ok(("", Line::Empty)));
}

#[test]
fn it_should_parse_file_comments() {
    assert_eq!(
        line(";2017-07-07 (ok) 'From a to b   "),
        Ok(("", Line::FileComment("2017-07-07 (ok) 'From a to b")))
    );
}

#[test]
fn it_should_parse_transaction_header() {
    use crate::{datetime::DateTime, status_mark::StatusMark};
    use chrono::NaiveDate;
    assert_eq!(
        line("2017-07-07 (ok) 'From a to b   "),
        Ok((
            "",
            Line::TransactionHeader(TransactionHeader {
                datetime: DateTime::Date(NaiveDate::from_ymd(2017, 7, 7)),
                status_mark: StatusMark::Unmarked,
                code: Some("ok"),
                description: "From a to b"
            })
        ))
    );
}

#[test]
fn it_should_parse_transaction_comment() {
    assert_eq!(
        line(" ; This is a transaction comment  "),
        Ok((
            "",
            Line::TransactionComment(Comment::UserComment("This is a transaction comment"))
        ))
    );

    assert_eq!(
        line("    # uuid: 83976d4b-8ea8-4cec-804f-931e4f171c3b"),
        Ok((
            "",
            Line::TransactionComment(Comment::Attribute(
                "uuid: 83976d4b-8ea8-4cec-804f-931e4f171c3b"
            ))
        ))
    );
}

#[test]
fn it_should_parse_posting() {
    use crate::{
        amount::Amount, position::Position, posting_type::PostingType, posting_value::PostingValue,
    };
    assert_eq!(
        line(" Expenses:Ice·Cream  200 SEK @ 0.1039 EUR"),
        Ok((
            "",
            Line::Posting(Posting {
                account_name: "Expenses:Ice·Cream",
                posting_type: PostingType::RegularPosting,
                posting_value: Some(PostingValue {
                    value_position: Position {
                        amount: Amount::Numeric("200"),
                        commodity: Some("SEK")
                    },
                    closing_unit_price: Some(Position {
                        amount: Amount::Numeric("0.1039"),
                        commodity: Some("EUR")
                    }),
                    closing_total_price: None,
                }),
                comment: None
            })
        ))
    );
}

#[test]
fn it_should_fail_to_parse_posting_if_not_indented() {
    assert!(line("Expenses:Ice·Cream  200 SEK @ 0.1039 EUR").is_err());
}

#[test]
fn it_should_fail_to_parse_transaction_header_if_indented() {
    assert!(line(" 2017-07-07 (ok) 'From a to b   ").is_err());
}
