use crate::helpers::{
    character_complete_ext::{alpha_char, take_while_cond},
    symbols::{is_currency_symbol, is_interpunct},
};
use nom::{
    branch::alt,
    character::complete::anychar,
    combinator::{recognize, verify},
    sequence::tuple,
    IResult,
};

// Commodities are currencies like $, €, EUR, USD, or a commodity name + unit (e.g. g·Ag)
pub(crate) fn commodity(i: &str) -> IResult<&str, &str> {
    alt((textual_commodity, currency_symbol))(i)
}

fn textual_commodity(i: &str) -> IResult<&str, &str> {
    recognize(tuple((
        alpha_char,
        take_while_cond(|c: char| c.is_alphabetic() || is_interpunct(c)),
    )))(i)
}

fn currency_symbol(i: &str) -> IResult<&str, &str> {
    recognize(verify(anychar, |c: &char| is_currency_symbol(*c)))(i)
}

#[test]
fn it_should_parse_valid_ascii_commodities() {
    let valid = ["EUR", "USD"];

    for &v in &valid {
        assert_eq!(commodity(v), Ok(("", v)));
    }
}

#[test]
fn it_should_parse_valid_utf8_commodities() {
    let valid = ["ACME·INC", "Ä·INC"];

    for &v in &valid {
        assert_eq!(commodity(v), Ok(("", v)));
    }
}

#[test]
fn it_should_parse_valid_currency_signs() {
    let valid = ["€", "$", "¥", "$"];

    for &v in &valid {
        assert_eq!(commodity(v), Ok(("", v)));
    }
}

#[test]
fn it_should_fail_on_invalid_commodities() {
    let invalid = ["·EUR", "%", "_EUR", "-EUR"];

    for &v in &invalid {
        assert!(commodity(v).is_err());
    }
}
