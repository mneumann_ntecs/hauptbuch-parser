#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum PostingType {
    /// A regular "balancing" posting
    RegularPosting,
    /// A virtual "non-balancing" posting
    VirtualPosting,
}
