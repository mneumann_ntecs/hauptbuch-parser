# hauptbuch-parser

Journal file parser for hauptbuch.

The format used for the journal is largely based on the format used by
[Tackler][tackler] ([see journal format][tackler-journal]).

[tackler]: https://gitlab.com/e257/accounting/tackler
[tackler-journal]: https://tackler.e257.fi/docs/journal/format/
