use criterion::{criterion_group, criterion_main, Benchmark, Criterion, Throughput};
use hauptbuch_parser::line::line as parse_line;

const LINES: &[&'static str] = &[
    "2016-01-01T00:00:06Z (#0000002) 'txn-2\n",
    " e:ey2016:em01:ed01  1.0000001\n",
    " a:ay2016:am01\n",
];

fn parse_lines(lines: &[&str]) {
    for line in lines {
        assert!(parse_line(line).is_ok());
    }
}

fn benchmark(c: &mut Criterion) {
    c.bench(
        "line_parser",
        Benchmark::new("nom", |b| b.iter(|| parse_lines(LINES)))
            .throughput(Throughput::Elements(LINES.len() as u32)),
    );
}

criterion_group!(benches, benchmark);
criterion_main!(benches);
