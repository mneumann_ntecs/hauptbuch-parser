use hauptbuch_parser::line::line as parse_line;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[test]
fn test_parse_file() {
    let file = File::open("tests/ok/001.txn").unwrap();
    let buf_reader = BufReader::new(file);

    for line in buf_reader.lines() {
        let line = line.unwrap();
        let result = parse_line(&line);
        assert!(result.is_ok());
    }
}
